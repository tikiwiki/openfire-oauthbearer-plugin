# OAuthBearer

This plugin enables Openfire to receive an OAuthBearer Token via SASL
protocol to authenticate a registered user.

The token must be in JWT standard and the validation is done by a hashing
algorithm. Currently, the supported hashing algorithms are **HMAC256**,
**HMAC384** and **HMAC512**.


## Configuring

The token is validate by using a secret key. Each OAuth client trying to access
Openfire has a **client id** and a **secret key**. This information must be
provided on Openfire admin panel at http://localhost:9090/plugins/oauthbearer/oauthbearer.jsp,
or navigating *Server* > *Server Settings* > *Oauth Bearer Token*

## Example

The following piece to be submitted to Openfire uses SASL protocol and set the
mechanism to `OAUTHBEARER`.

```xml
<body rid="685064415" sid="4ci8dpnx5n" xmlns="http://jabber.org/protocol/httpbind">
    <auth mechanism="OAUTHBEARER" xmlns="urn:ietf:params:xml:ns:xmpp-sasl">
        bixhPWZhYmlvQG9wZW5maXJlLmRvY2tlciwBYXV0aD1CZWFyZXIgZXlKMGVYQWlPaUpLVj
        FRaUxDSmhiR2NpT2lKSVV6STFOaUlzSW1wMGFTSTZJbU0yTmpkak1qTmhPRFl3WldGbE4y
        RmhPVEZtWVRVM1ltVXlPVE16T0RVMUluMC5leUpoZFdRaU9pSnZjbWN1ZEdscmFTNXlkR0
        11YVc1MFpYSnVZV3d0WTI5dWRtVnljMlZxY3kxcFpDSXNJbXAwYVNJNkltTTJOamRqTWpO
        aE9EWXdaV0ZsTjJGaE9URm1ZVFUzWW1VeU9UTXpPRFUxSWl3aWFXRjBJam94TlRVMk1UTT
        VOelU1TENKdVltWWlPakUxTlRZeE16azNOVGtzSW1WNGNDSTZNVFUxTmpFME16TTFPU3dp
        YzNWaUlqb2labUZpYVc4aUxDSnpZMjl3WlhNaU9sdGRmUS5BY3h5T1c4WWU0TjFGZ2tjem
        9keXZ3TFYxX0liRHgwZVBvUWNwdzhFWmFFAQE=
    </auth>
</body>
```
