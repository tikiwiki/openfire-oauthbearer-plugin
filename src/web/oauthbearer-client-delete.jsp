<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page
    import="java.util.*,
                org.jivesoftware.openfire.XMPPServer,
                org.jivesoftware.util.*,
                org.jivesoftware.openfire.container.Plugin,
                org.jivesoftware.openfire.container.PluginManager,
                org.tiki.oauthbearer.client.ClientEntity,
                org.tiki.oauthbearer.client.ClientRepository,
                org.tiki.oauthbearer.web.ClientValidator"
    errorPage="error.jsp"%>
<%@ page import="org.jivesoftware.openfire.container.PluginMetadataHelper" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%-- Define Administration Bean --%>
<jsp:useBean id="admin" class="org.jivesoftware.util.WebManager" />
<c:set var="admin" value="${admin.manager}" />
<%
    admin.init(request, response, session, application, out);

    String clientId = Objects.toString(ParamUtils.getParameter(request, "clientId"), "");
    ClientRepository repo = new ClientRepository();
    ClientEntity client = new ClientEntity();

    try {
        client = repo.getClient(clientId);
    } catch(Exception ex) { }

    if (request.getMethod() == "POST") {
        repo.deleteClient(client);
        response.sendRedirect("oauthbearer.jsp?message=client-deleted");
    }
%>
<html>
    <head>
        <title>Delete <%= Objects.toString(client.getName(), "") %></title>
        <meta name="pageID" content="oauthbearer"/>
        <link rel="stylesheet" href="assets/styles.css" />
        <script type="text/javascript" src="/js/prototype.js"></script>
    </head>
    <body>
        <div id="oauthbearer-plugin">
            <form method="post" action="?">

                <div class="warning-box">
                    <p><span class="ico ico-warning"></span>You are up to delete this resource, are you sure of it?</p>
                </div>

                <div class="client">
                    <h3><%= Objects.toString(client.getName(), "") %></h3>
                    <input type="hidden" name="clientId" value="<%= client.getClientId() %>" />

                    <table class="table">
                        <tr>
                            <th>Client ID</th>
                            <td><%= Objects.toString(client.getClientId(), "") %></td>
                        </tr>
                        <tr>
                            <th>Client Secret</th>
                            <td><%= Objects.toString(client.getClientSecret(), "") %></td>
                        </tr>
                    </table>
                </div>
                <hr />

                <div class="btn-group text-center">
                    <button class="btn btn-danger" type="submit">Delete</button>
                    <a class="btn" href="oauthbearer-client-edit.jsp?clientId=<%= client.getClientId() %>">Edit</a>
                    <a class="btn" href="oauthbearer.jsp">Cancel</a>
                </div>
            </form>
        </div>
    </body>
</html>