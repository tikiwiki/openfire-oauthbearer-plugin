<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page
    import="java.util.*,
                org.jivesoftware.openfire.XMPPServer,
                org.jivesoftware.util.*,
                org.jivesoftware.openfire.container.Plugin,
                org.jivesoftware.openfire.container.PluginManager,
                org.tiki.oauthbearer.client.ClientEntity,
                org.tiki.oauthbearer.client.ClientRepository"
    errorPage="error.jsp"%>
<%@ page import="org.jivesoftware.openfire.container.PluginMetadataHelper" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%-- Define Administration Bean --%>
<jsp:useBean id="admin" class="org.jivesoftware.util.WebManager" />
<c:set var="admin" value="${admin.manager}" />
<%
    admin.init(request, response, session, application, out);
    ClientRepository repo = new ClientRepository();
    List<ClientEntity> clientList = repo.listClients();

%>
<html>
    <head>
        <title>OAuth Bearer Token</title>
        <meta name="pageID" content="oauthbearer"/>
        <link rel="stylesheet" href="assets/styles.css" />
        <script type="text/javascript" src="/js/prototype.js"></script>
    </head>
    <body>
        <div id="oauthbearer-plugin">
            <p>
                Here you can list and find trusted OAuth providers. It means registered users on those providers will be
                able to login into Openfire without using password. But to use this way, end users will need an XMPP client
                capable to contact an OAuth provider, fetch authorization and give this authorization to Openfire check
                on provider.
            </p>

            <% if (clientList.isEmpty()) { %>
                <p>
                    You don't have any client registered yet. Click to create a new one
                    <a class="btn" href="oauthbearer-client-edit.jsp">Create</a>
                </p> 

            <% } else { %>
            
            <div class="clearfix">
                <a style="float: right" class="btn" href="oauthbearer-client-edit.jsp">Add</a>
                <h2>Clients list</h2>
            </div>

            <div class="client-list">
                <% for (ClientEntity client : repo.listClients()) { %>
                <div class="client">
                    <h3><%= Objects.toString(client.getName(), "") %></h3>
                    <table class="table">
                        <tr>
                            <th>Client ID</th>
                            <td><%= Objects.toString(client.getClientId(), "") %></td>
                        </tr>
                        <tr>
                            <th>Client Secret</th>
                            <td><%= Objects.toString(client.getClientSecret(), "") %></td>
                        </tr>
                    </table>
                    <div class="client-actions">
                        <form action="oauthbearer-client-delete.jsp" class="btn-group" method="post">
                            <a class="btn btn-danger" href="oauthbearer-client-delete.jsp?clientId=<%= client.getClientId() %>">Delete</a>
                            <a class="btn" href="oauthbearer-client-edit.jsp?clientId=<%= client.getClientId() %>">Edit</a>
                        </form>
                    </div>
                </div>
                <% } %>
            </div>
            <% } %>
        </div>
    </body>
</html>
