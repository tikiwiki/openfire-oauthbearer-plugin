<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page
    import="java.util.*,
                org.jivesoftware.openfire.XMPPServer,
                org.jivesoftware.util.*,
                org.jivesoftware.openfire.container.Plugin,
                org.jivesoftware.openfire.container.PluginManager,
                org.tiki.oauthbearer.client.ClientEntity,
                org.tiki.oauthbearer.client.ClientRepository,
                org.tiki.oauthbearer.web.ClientValidator"
    errorPage="error.jsp"%>
<%@ page import="org.jivesoftware.openfire.container.PluginMetadataHelper" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%-- Define Administration Bean --%>
<jsp:useBean id="admin" class="org.jivesoftware.util.WebManager" />
<c:set var="admin" value="${admin.manager}" />
<%
    admin.init(request, response, session, application, out);

    String clientId = Objects.toString(ParamUtils.getParameter(request, "clientId"), "");
    String originalClientId = Objects.toString(ParamUtils.getParameter(request, "originalClientId"), "");

    if (request.getMethod() == "GET") {
        originalClientId = clientId;
    }

    ClientRepository repo = new ClientRepository();
    ClientEntity client = new ClientEntity();

    try {
        if ( !clientId.isEmpty() ) {
            if ( !originalClientId.isEmpty() && !originalClientId.equals(clientId) ) {
                client = repo.getClient(originalClientId);
            } else {
                client = repo.getClient(clientId);
            }
        }
    } catch(Exception ex) {

    }

    ClientValidator validator = new ClientValidator(client);

    if (request.getMethod() == "POST") {
        client.setName( ParamUtils.getParameter(request, "name") );
        client.setClientId( ParamUtils.getParameter(request, "clientId") );
        client.setClientSecret( ParamUtils.getParameter(request, "clientSecret") );

        if(validator.validate()) {
            if (originalClientId.isEmpty()) {
                repo.createClient(client);
            } else {
                repo.updateClient(client, originalClientId);
            }
            
            response.sendRedirect("oauthbearer.jsp?message=client-saved");
        }
        validator.getErrors("clientId");
    }

    
%>
<html>
    <head>
        <title>Edit <%= Objects.toString(client.getName(), "") %></title>
        <meta name="pageID" content="oauthbearer"/>
        <link rel="stylesheet" href="assets/styles.css" />
        <script type="text/javascript" src="/js/prototype.js"></script>
    </head>
    <body>
        <div id="oauthbearer-plugin">
            <form method="post" action="?">
                <input type="hidden" name="originalClientId" value="<%= originalClientId %>" />

                <div class="form-group">
                    <label for="field-name">Name</label>
                    <input  id="field-name" name="name" value="<%= Objects.toString(client.getName(), "") %>" class="form-control" type="text" />
                    
                    <% for (String error : validator.getErrors("name")) { %>
                        <p class="text-error"><%= error %></p>
                    <% } %>
                </div>

                <div class="form-group">
                    <label for="field-clientId">ClientId</label>
                    <input  id="field-clientId" name="clientId" value="<%= Objects.toString(client.getClientId(), "") %>" class="form-control" type="text" />
                    
                    <% for (String error : validator.getErrors("clientId")) { %>
                        <p class="text-error"><%= error %></p>
                    <% } %>
                </div>

                <div class="form-group">
                    <label for="field-clientSecret">ClientSecret</label>
                    <input  id="field-clientSecret" name="clientSecret" value="<%= Objects.toString(client.getClientSecret(), "") %>" class="form-control" type="text" />
                    
                    <% for (String error : validator.getErrors("clientSecret")) { %>
                        <p class="text-error"><%= error %></p>
                    <% } %>
                </div>

                <div class="form-group">
                    <div class="btn-group text-right">
                        <a class="btn" href="oauthbearer.jsp">Cancel</a>
                        <button class="btn" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>