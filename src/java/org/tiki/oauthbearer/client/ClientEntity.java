package org.tiki.oauthbearer.client;

import java.net.MalformedURLException;
import java.net.URL;

public class ClientEntity {
    private String name;
    private String clientId;
    private String clientSecret;

    public ClientEntity()
    {
        this.setName("");
        this.setClientId("");
        this.setClientSecret("");
    }

    public ClientEntity(String name, String clientId, String clientSecret, URL validator)
    {
        this.setName(name);
        this.setClientId(clientId);
        this.setClientSecret(clientSecret);
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getClientId()
    {
        return this.clientId;
    }

    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }

    public String getClientSecret()
    {
        return this.clientSecret;
    }

    public void setClientSecret(String clientSecret)
    {
        this.clientSecret = clientSecret;
    }
}