package org.tiki.oauthbearer.client;

import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.util.NotFoundException;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

public class ClientRepository
{
    static final String CREATE_CLIENT_SQL = "INSERT INTO ofOauthbearer ("
        +    "name, clientId, clientSecret"
        + ") VALUES(?, ?, ?)";

    static final String UPDATE_CLIENT_SQL = "UPDATE ofOauthbearer"
        +    " SET name=?, clientId=?, clientSecret=?"
        +    " WHERE clientId=?";

    static final String LIST_ALL_CLIENTS_SQL = "SELECT"
        +    " name, clientId, clientSecret"
        + " FROM ofOauthbearer";

    static final String GET_CLIENT_BY_ID_SQL = "SELECT"
        +    " name, clientId, clientSecret"
        + " FROM ofOauthbearer WHERE clientId=?";

    static final String DELETE_CLIENT_SQL = "DELETE FROM ofOauthbearer"
        + " WHERE clientId=?";


    public ClientEntity getClient(String clientId) throws SQLException, NotFoundException
    {
        boolean abort = false;
        Connection conn = null;
        ClientEntity client = null;
        ResultSet result = null;

        try {
            conn = DbConnectionManager.getTransactionConnection();
            PreparedStatement stmt = conn.prepareStatement(GET_CLIENT_BY_ID_SQL);
            stmt.setString(1, Objects.toString(clientId, ""));
            result = stmt.executeQuery();

            if (!result.next()) {
                throw new NotFoundException("Client(clientId=" + clientId +") does not exist");
            }

            client = new ClientEntity();
            client.setName(result.getString(1));
            client.setClientId(result.getString(2));
            client.setClientSecret(result.getString(3));

            stmt.close();
        } catch(SQLException ex) {
            abort = true;
            throw ex;
        } finally {
            DbConnectionManager.closeTransactionConnection(conn, abort);
        }

        return client;
    }

    public ArrayList<ClientEntity> listClients() throws SQLException
    {
        boolean abort = false;
        Connection conn = null;
        ArrayList<ClientEntity> clients = new ArrayList<ClientEntity>();

        ClientEntity client;
        ResultSet result;

        try {
            conn = DbConnectionManager.getTransactionConnection();
            PreparedStatement stmt = conn.prepareStatement(LIST_ALL_CLIENTS_SQL);
            result = stmt.executeQuery();

            while (result.next()) {
                client = new ClientEntity();
                client.setName(result.getString(1));
                client.setClientId(result.getString(2));
                client.setClientSecret(result.getString(3));
                clients.add(client);
            }

            stmt.close();
        } catch(SQLException ex) {
            abort = true;
            throw ex;
        } finally {
            DbConnectionManager.closeTransactionConnection(conn, abort);
        }

        return clients;
    }
    
    public void createClient(ClientEntity client) throws SQLException
    {
        Connection conn = null;
        boolean abort = false;
        try {
            conn = DbConnectionManager.getTransactionConnection();
            PreparedStatement stmt = conn.prepareStatement(CREATE_CLIENT_SQL);
            stmt.setString(1, Objects.toString(client.getName(), ""));
            stmt.setString(2, Objects.toString(client.getClientId(), ""));
            stmt.setString(3, Objects.toString(client.getClientSecret(), ""));
            stmt.executeUpdate();
            stmt.close();
        } catch(SQLException ex) {
            abort = true;
            throw ex;
        } finally {
            DbConnectionManager.closeTransactionConnection(conn, abort);
        }
    }

    public void updateClient(ClientEntity client, String originalClientId) throws SQLException
    {
        Connection conn = null;
        boolean abort = false;
        try {
            conn = DbConnectionManager.getTransactionConnection();
            PreparedStatement stmt = conn.prepareStatement(UPDATE_CLIENT_SQL);
            stmt.setString(1, Objects.toString(client.getName(), ""));
            stmt.setString(2, Objects.toString(client.getClientId(), ""));
            stmt.setString(3, Objects.toString(client.getClientSecret(), ""));
            stmt.setString(4, Objects.toString(originalClientId, ""));
            stmt.executeUpdate();
            stmt.close();
        } catch(SQLException ex) {
            abort = true;
            throw ex;
        } finally {
            DbConnectionManager.closeTransactionConnection(conn, abort);
        }
    }

    public void deleteClient(ClientEntity client) throws SQLException
    {
        Connection conn = null;
        boolean abort = false;
        try {
            conn = DbConnectionManager.getTransactionConnection();
            PreparedStatement stmt = conn.prepareStatement(DELETE_CLIENT_SQL);
            stmt.setString(1, client.getClientId());
            stmt.executeUpdate();
            stmt.close();
        } catch(SQLException ex) {
            abort = true;
            throw ex;
        } finally {
            DbConnectionManager.closeTransactionConnection(conn, abort);
        }
    }
}