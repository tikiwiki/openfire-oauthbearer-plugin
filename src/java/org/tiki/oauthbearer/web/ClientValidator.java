package org.tiki.oauthbearer.web;

import org.tiki.oauthbearer.client.ClientEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ClientValidator extends Validator<ClientEntity>
{
    private ClientEntity client;
    private List<Validator> validators;

    public ClientValidator(ClientEntity client)
    {
        super();
        this.setSubject(client);
    }

    public boolean validate()
    {
        boolean valid = true;
        ClientEntity subject = this.getSubject();
        
        if (subject == null) {
            this.addError("global", "No client info provided");
            return false;
        }

        if (subject.getName() == null || subject.getName().isEmpty()){
            this.addError("name", "Field 'name' is required");
            valid = false;
        }

        if (subject.getClientId() == null || subject.getClientId().isEmpty()){
            this.addError("clientId", "Field 'clientId' is required");
            valid = false;
        }

        if (subject.getClientSecret() == null || subject.getClientSecret().isEmpty()){
            this.addError("clientSecret", "Field 'clientSecret' is required");
            valid = false;
        }

        return valid;
    }
}
