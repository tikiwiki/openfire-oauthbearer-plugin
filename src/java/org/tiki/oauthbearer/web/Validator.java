package org.tiki.oauthbearer.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Validator<T>
{
    protected HashMap<String, List<String>> errors;
    protected T subject;

    public Validator ()
    {
        this.errors = new HashMap<String, List<String>>();
    }

    public Validator (T subject)
    {
        this();
        setSubject(subject);
    }

    public boolean hasErrors ()
    {
        return this.errors.size() > 0;
    }

    public void addError (String topic, String message)
    {
        List<String> topicErrors = this.errors.get(topic);
        if ( topicErrors == null )
        {
            topicErrors = new ArrayList<String>();
            this.errors.put(topic, topicErrors);
        }
        topicErrors.add(message);
    }

    public List<String> getErrors (String topic)
    {
        List<String> topicErrors = this.errors.get(topic);
        return topicErrors == null ? new ArrayList<String>() : topicErrors;
    }

    public HashMap<String, List<String>> getErrors ()
    {
        return this.errors;
    }

    public T getSubject ()
    {
        return this.subject;
    }

    public void setSubject (T subject)
    {
        this.subject = subject;
    }

    public boolean validate (T subject)
    {
        this.setSubject(subject);
        return this.validate();
    }

    public abstract boolean validate ();

}
