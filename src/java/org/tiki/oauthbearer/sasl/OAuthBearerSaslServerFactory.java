package org.tiki.oauthbearer.sasl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.callback.CallbackHandler;
import javax.security.sasl.Sasl;
import javax.security.sasl.SaslException;
import javax.security.sasl.SaslServer;
import javax.security.sasl.SaslServerFactory;
import java.util.Map;

public class OAuthBearerSaslServerFactory implements SaslServerFactory
{
    private static final Logger Log = LoggerFactory.getLogger( OAuthBearerSaslServerFactory.class );

    @Override
    public SaslServer createSaslServer(String mechanism, String protocol, String serverName, Map<String, ?> props, CallbackHandler cbh) throws SaslException
    {
        if ( hasAcceptableProps(props) ) {
            Log.info( "Instantiating a new OAuthBearerSaslServer instance." );
            return new OAuthBearerSaslServer();
        }

        Log.info( "Unable to instantiate OAuthBearerSaslServer with requested properties." );
        return null;
    }


    @Override
    public String[] getMechanismNames(Map<String, ?> props)
    {
        if ( hasAcceptableProps(props) ) {
            return new String[]{ OAuthBearerSaslServer.MECHANISM_NAME };
        }
        Log.info( "Unable to list OAuthBearerSaslServer with requested properties." );
        return new String[0];
    }

    protected boolean hasAcceptableProps(Map<String, ?> props)
    {
        if (props == null) {
            return true;
        }

        for ( Map.Entry<String, ?> entry: props.entrySet() ) {
            if (! (entry.getValue() instanceof String)) {
                continue;
            }

            final String name = entry.getKey();
            final String value = (String) entry.getValue();

            if (Sasl.POLICY_NOPLAINTEXT.equalsIgnoreCase( name ) && "true".equalsIgnoreCase( value )) {
                return false;
            }
        }

        return true;
    }
}
