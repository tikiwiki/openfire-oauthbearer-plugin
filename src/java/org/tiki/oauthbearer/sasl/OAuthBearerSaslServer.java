package org.tiki.oauthbearer.sasl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tiki.oauthbearer.token.OAuthBearerEntity;
import org.tiki.oauthbearer.token.OAuthBearerEntityValidator;
import org.tiki.oauthbearer.web.Validator;

import javax.security.sasl.Sasl;
import javax.security.sasl.SaslException;
import javax.security.sasl.SaslServer;

public class OAuthBearerSaslServer implements SaslServer
{
    private final static Logger Log = LoggerFactory.getLogger(OAuthBearerSaslServer.class);
    public static final String MECHANISM_NAME = "OAUTHBEARER";


    private enum State
    {

        /**
         * Initial state. Has not evaluated any response yet.
         */
        PRE_INITIAL_RESPONSE,

        /**
         * Has evaluated an initial response, but has not yet completed.
         */
        POST_INITIAL_RESPONSE,

        /**
         * Done (authentication succeeded or failed).
         */
        COMPLETED
    }


    ;
    private State state = State.PRE_INITIAL_RESPONSE;
    private String authorizationId;


    public OAuthBearerSaslServer ()
    {
        Log.info("Created OAuthBearerSaslServer");
    }

    @Override
    public String getMechanismName ()
    {
        return MECHANISM_NAME;
    }

    @Override
    public byte[] evaluateResponse (byte[] bytes) throws SaslException
    {
        if ( isComplete() )
        {
            throw new IllegalStateException("OAUTHBEARER authentication was already completed.");
        }
        Log.trace("Current state: {}", state);

        if ( bytes == null || bytes.length == 0 )
        {
            if ( state == State.POST_INITIAL_RESPONSE )
            {
                state = State.COMPLETED;
                throw new SaslException("The OAUTHBEARER SASL mechanism expects response data in either the initial or second client response. Neither had any data.");
            }

            if ( state == State.PRE_INITIAL_RESPONSE )
            {
                state = State.POST_INITIAL_RESPONSE;
                return null;
            }

            throw new IllegalStateException("Instance is in an unrecognized state (please report this incident as a bug in class: " + this.getClass().getCanonicalName() + "). Unrecognized value: " + state);
        }

        state = State.COMPLETED;
        OAuthBearerEntity entity = OAuthBearerSaslContentParser.parse(bytes);

        Validator<OAuthBearerEntity> validator = new OAuthBearerEntityValidator(entity);

        if ( validator.validate() )
        {
            this.authorizationId = entity.getA();
            return new byte[ 0 ];
        }

        throw new IllegalStateException("The message was reject by unknown reason");
    }

    @Override
    public boolean isComplete ()
    {
        return state == State.COMPLETED;
    }

    @Override
    public String getAuthorizationID ()
    {
        return authorizationId;
    }

    @Override
    public byte[] unwrap (byte[] incoming, int offset, int len) throws SaslException
    {
        if ( !isComplete() )
        {
            throw new IllegalStateException("OAUTHBEARER authentication has not completed.");
        }

        throw new IllegalStateException("OAUTHBEARER supports neither integrity nor privacy.");
    }

    @Override
    public byte[] wrap (byte[] outgoing, int offset, int len) throws SaslException
    {
        if ( !isComplete() )
        {
            throw new IllegalStateException("OAUTHBEARER authentication has not completed.");
        }

        throw new IllegalStateException("OAUTHBEARER supports neither integrity nor privacy.");
    }

    @Override
    public Object getNegotiatedProperty (String propName)
    {
        if ( !isComplete() )
        {
            throw new IllegalStateException("OAUTHBEARER authentication has not completed.");
        }

        if ( Sasl.QOP.equals(propName) )
        {
            return "auth";
        }
        return null;
    }

    @Override
    public void dispose () throws SaslException
    {
        state = null;
        authorizationId = null;
    }
}
