package org.tiki.oauthbearer.sasl;

import java.security.Provider;

public class OAuthBearerSaslProvider extends Provider
{
    public static final String NAME = "OAuthBearerSasl";
    public static final double VERSION = 1.0;
    public static final String INFO = "Provides a SASL mechanism that uses JWT token standard.";

    public OAuthBearerSaslProvider( )
    {
        super(NAME, VERSION, INFO);
        put( "SaslServerFactory." + OAuthBearerSaslServer.MECHANISM_NAME, OAuthBearerSaslServerFactory.class.getCanonicalName() );
    }
}
