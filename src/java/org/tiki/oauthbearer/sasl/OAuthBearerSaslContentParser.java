package org.tiki.oauthbearer.sasl;

import org.tiki.oauthbearer.token.OAuthBearerEntity;

public class OAuthBearerSaslContentParser
{

    private int pos;
    private byte[] data;
    private OAuthBearerEntity entity;

    public OAuthBearerSaslContentParser(byte[] data)
    {
        this.data = data;
        this.entity = new OAuthBearerEntity();
    }

    private boolean hasNext()
    {
        return this.pos < data.length;
    }

    private char next()
    {
        return (char) this.data[ ++this.pos ];
    }

    private char current()
    {
        return (char) this.data[ this.pos ];
    }

    private boolean eof()
    {
        if (this.pos + 1 < this.data.length)
        {
            char c = current();
            char n = (char) this.data[this.pos + 1];

            return c == '\1' && n == '\1';
        }
        throw new IllegalStateException("Illegal end of Sasl content");
    }

    private void parseParameter() throws IllegalStateException
    {
        parseParameter('\1');
    }

    private void parseParameter(char delim) throws IllegalStateException
    {
        StringBuilder key = new StringBuilder();
        StringBuilder value = new StringBuilder();

        while(next() != '=')
        {
            if( !Character.isLetterOrDigit(current()) )
            {
                throw new IllegalStateException(String.format("Invalid symbol at %d", this.pos));
            }

            key.append(current());
        }

        if (!hasNext())
        {
            throw new IllegalStateException(String.format("Premature end of content at %d", this.pos));
        }

        while(next() != delim)
        {
            value.append(current());
        }

        entity.set(key.toString(), value.toString());
    }

    private void parseUser() throws IllegalStateException
    {
        boolean valid = current() == 'n'
                && next() == ',';

        if (!valid)
        {
            throw new IllegalStateException("Sasl content should start with 'n,'");
        }

        parseParameter(',');

        if (next() != '\1') {
            throw new IllegalStateException(String.format("Premature end of content at %d", this.pos));
        }
    }

    public OAuthBearerEntity parseContent() throws IllegalStateException
    {
        if (!hasNext()) {
            throw new IllegalStateException("Empty Sasl content");
        }

        parseUser();
        while(!eof())
        {
            parseParameter();
        }

        return this.entity;
    }

    public static OAuthBearerEntity parse(byte[] data) throws IllegalStateException
    {
        OAuthBearerSaslContentParser p = new OAuthBearerSaslContentParser(data);
        return p.parseContent();
    }
}
