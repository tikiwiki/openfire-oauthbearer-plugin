package org.tiki.oauthbearer.token;

import java.util.HashMap;

public class OAuthBearerEntity
{
    private HashMap<String, String> fields;

    public OAuthBearerEntity ()
    {
        fields = new HashMap<>();
        setA("");
        setAuth("");
        setMthd("POST");
        setPath("/");
        setPost("");
        setQs("");
    }

    public String get(String field)
    {
        return this.fields.get(field);
    }

    public void set(String field, String value)
    {
        if (!this.fields.containsKey(field))
        {
            throw new IllegalStateException(String.format("Field '%s' does not exists in rfc7628", field));
        }
        this.fields.put(field, value);
    }

    public String getA ()
    {
        return this.fields.get("a");
    }

    public void setA (String a)
    {
        this.fields.put("a", a);
    }

    public String getAuth ()
    {
        return this.fields.get("auth");
    }

    public void setAuth (String auth)
    {
        this.fields.put("auth", auth);
    }

    public String getMthd ()
    {
        return this.fields.get("mthd");
    }

    public void setMthd (String mthd)
    {
        this.fields.put("mthd", mthd);
    }

    public String getPath ()
    {
        return this.fields.get("path");
    }

    public void setPath (String path)
    {
        this.fields.put("path", path);
    }

    public String getPost ()
    {
        return this.fields.get("post");
    }

    public void setPost (String post)
    {
        this.fields.put("post", post);
    }

    public String getQs ()
    {
        return this.fields.get("qs");
    }

    public void setQs (String qs)
    {
        this.fields.put("qs", qs);
    }
}
