package org.tiki.oauthbearer.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.jivesoftware.util.NotFoundException;
import org.tiki.oauthbearer.client.ClientEntity;
import org.tiki.oauthbearer.client.ClientRepository;
import org.tiki.oauthbearer.web.Validator;

import java.sql.SQLException;
import java.util.Date;


public class OAuthBearerEntityValidator extends Validator<OAuthBearerEntity>
{
    public OAuthBearerEntityValidator ()
    {
        super();
    }

    public OAuthBearerEntityValidator (OAuthBearerEntity entity)
    {
        super(entity);
    }

    @Override
    public boolean validate ()
    {
        boolean valid = true;

        OAuthBearerEntity entity = getSubject();

        if ( entity == null )
        {
            throw new IllegalStateException("Invalid token");
        }

        if ( entity.getA() == null || entity.getA().isEmpty() )
        {
            addError("a", "'a' field is required");
            valid = false;
        }

        if ( entity.getAuth() == null || entity.getAuth().isEmpty() )
        {
            throw new IllegalStateException("'auth' field cannot be empty");
        }

        String token = entity.getAuth();
        if ( !token.startsWith("Bearer ") )
        {
            throw new IllegalStateException("'auth' should start with 'Bearer '");
        }

        token = token.replaceFirst("^Bearer  *", "");

        JWT jwtLib;
        DecodedJWT decodedJWT;

        try
        {
            jwtLib = new JWT();
            decodedJWT = jwtLib.decodeJwt(token);
        }
        catch ( JWTDecodeException exc )
        {
            throw new IllegalStateException(exc.getMessage());
        }

        if ( decodedJWT.getExpiresAt().before(new Date()) )
        {
            throw new IllegalStateException("Token expired");
        }

        // We use 'aud' to select the registered client in Openfire
        if ( decodedJWT.getAudience().isEmpty() )
        {
            throw new IllegalStateException("'aud' must contain the clientId");
        }

        String clientId = decodedJWT.getAudience().get(0);
        ClientRepository clientRepo = new ClientRepository();

        // Check if we really have this Client registered
        ClientEntity client;
        try
        {
            client = clientRepo.getClient(clientId);
        }
        catch ( SQLException ex )
        {
            throw new IllegalStateException("Internal error");
        }
        catch ( NotFoundException ex )
        {
            throw new IllegalStateException("'clientId not found");
        }

        // Check if provider provided the algorithm to be used
        String algorithmId = decodedJWT.getAlgorithm();
        if ( algorithmId == null || algorithmId.isEmpty() )
        {
            throw new IllegalStateException("'algorithm' should be informed for signature check");
        }

        // Select one hashing algorithm. I don't see how to add key based algorithms here
        Algorithm algorithm;
        switch ( algorithmId.toUpperCase() )
        {
            case "HS256":
                algorithm = Algorithm.HMAC256(client.getClientSecret());
                break;
            case "HS384":
                algorithm = Algorithm.HMAC384(client.getClientSecret());
                break;
            case "HS512":
                algorithm = Algorithm.HMAC512(client.getClientSecret());
                break;
            default:
                throw new IllegalStateException("Algorithm not yet supported");
        }

        // Use the algorithm to check integrity
        try
        {
            JWTVerifier verifier = JWT.require(algorithm).build();
            decodedJWT.getSignature();

            verifier.verify(token);
        }
        catch ( JWTVerificationException ex )
        {
            throw new IllegalStateException("Signature is invalid");
        }

        return true;
    }
}
