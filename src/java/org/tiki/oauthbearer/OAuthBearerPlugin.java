package org.tiki.oauthbearer;

import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.net.SASLAuthentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.tiki.oauthbearer.sasl.OAuthBearerSaslProvider;
import org.tiki.oauthbearer.sasl.OAuthBearerSaslServer;

import java.io.File;
import java.security.Security;


public class OAuthBearerPlugin implements Plugin
{
    private static final Logger Log = LoggerFactory.getLogger( OAuthBearerPlugin.class );

    @Override
    public void initializePlugin( PluginManager manager, File pluginDirectory )
    {
        Log.info("Initializing OAuthBearerPlugin ");
        Security.addProvider(new OAuthBearerSaslProvider());
        SASLAuthentication.addSupportedMechanism( OAuthBearerSaslServer.MECHANISM_NAME );
    }

    @Override
    public void destroyPlugin()
    {
        SASLAuthentication.removeSupportedMechanism( OAuthBearerSaslServer.MECHANISM_NAME );
        Security.removeProvider( OAuthBearerSaslProvider.NAME );
    }
}