INSERT INTO `ofVersion` (name, version) VALUES ('oauthbearer', 0);

CREATE TABLE IF NOT EXISTS `ofOauthbearer` (
    `name`         VARCHAR(255) NOT NULL,
    `clientId`     VARCHAR(255) NOT NULL,
    `clientSecret` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`clientId`)
);
