# OAuthBearer changelog

* 0.2 - Apr 24, 2019
    * Fixed plugin.xml to instruct Openfire create needed table on plugin install
    * Remote an unused field from plugin
    * Added Readme and Changelog files
* 0.1 - Jan  4, 2019
    * Initial version of OAuthBearer plugin